#include <util/imageutil.h>

#include <opencv2/opencv.hpp>
#include <QImage>

namespace ImageUtil {
  QImage Mat2QImage(const cv::Mat &matsrc) {
    auto src = cv::Mat3b(matsrc);
    QImage dest(src.cols, src.rows, QImage::Format_ARGB32);
    for (auto y = 0; y < src.rows; ++y) {
      const cv::Vec3b *srcrow = src[y];
      auto *destrow = reinterpret_cast<QRgb*>(dest.scanLine(y));
      for (auto x = 0; x < src.cols; ++x) {
        destrow[x] = qRgba(srcrow[x][2], srcrow[x][1], srcrow[x][0], 255);
      }
    }
    return dest;
  }

  cv::Mat QImage2Mat(const QImage &src) {
    unsigned int height = src.height();
    unsigned int width = src.width();

    cv::Mat3b dest(height, width);
    for (unsigned int y = 0; y < height; ++y) {
	    auto destrow = dest[y];
      for (unsigned int x = 0; x < width; ++x) {
	      auto pxl = src.pixel(x, y);
        destrow[x] = cv::Vec3b(qBlue(pxl), qGreen(pxl), qRed(pxl));
      }
    }
    return cv::Mat(dest);
  }

}