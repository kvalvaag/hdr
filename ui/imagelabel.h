/*
 * imagelabel.h
 *
 *  Created on: Nov 18, 2012
 *      Author: petter
 */

#ifndef IMAGELABEL_H
#define IMAGELABEL_H

#include <QLabel>
#include <QWidget>

#include <ui_export.h>

class UI_EXPORT ImageLabel : public QLabel {
  Q_OBJECT
public:
  ImageLabel(QWidget * parent = 0);
  ~ImageLabel();

signals:
  void doubleClicked();

protected:
  void mouseDoubleClickEvent(QMouseEvent * e);

public:
  double GetX() const {
    return x_;
  }

  double GetY() const {
    return y_;
  }

private:
  double x_; // X coordinate of last double click
  double y_; // Y coordinate of last double click
};

#endif /* IMAGELABEL_H */
