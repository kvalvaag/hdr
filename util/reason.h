#ifndef UTIL_REASON_H
#define UTIL_REASON_H

#include <QString>

#include <util_export.h>

struct UTIL_EXPORT Reason {
  QString reason_;

  Reason(QString reason) {
    reason_ = reason;
  };

  QString GetReason() {
    return reason_;
  };

  bool operator==(Reason *reason) {
    return GetReason() == reason->GetReason();
  }

  bool operator==(Reason &reason) {
    return GetReason() == reason.GetReason();
  }
  bool operator==(Reason reason) {
    return GetReason() == reason.GetReason();
  }
};

#endif // UTIL_REASON_H
