/*
 * subject.cpp
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#include "util/subject.h"
#include "util/observer.h"

Subject::Subject() {
}

Subject::~Subject() {
}

void Subject::Attach(Observer *observer)
{
  observers_.push_back(observer);
}
void Subject::Detach(Observer *observer)
{
  observers_.erase(std::remove(observers_.begin(), observers_.end(), observer), observers_.end());
}

void Subject::Notify(Reason reason, UpdateInfo* info)
{
  for (Observer* observer : observers_) {
    if (observer != 0) {
      observer->Update(reason, info);
    }
  }
}
