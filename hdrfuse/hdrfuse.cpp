#include "hdrfuse.h"

#include <util/imageproxy.h>
#include <util/imageutil.h>
#include <util/compatibility.h>
#include <fuse/fuse.h>
#include <QImage>

#include <assert.h>
#include <opencv2/core/core.hpp>

HDRFuse::HDRFuse(QObject *parent) : QObject(parent),
                                    fuser_(std::make_unique<Fuse>()),
                                    settings_(std::make_unique<FuseSettings>())
{
}


HDRFuse::~HDRFuse()
{
}

void
HDRFuse::AddImage(ImageProxy* image) {
  images_.push_back(image);

  if (!images_mutex_.try_lock()) {
    return; //Let other instance of AddImage handle this one too
  }
  else {
    images_mutex_.unlock(); // Unlock and use exception safe locking
  }
  successes_.push_back(
    std::async(std::launch::async, &HDRFuse::ProcessImages, this));
}

bool
HDRFuse::ProcessImages() {
  std::lock_guard<std::mutex> lock(images_mutex_);
  while (!images_.empty()) {
    ImageProxy* im = *images_.begin();
    try {
      fuser_->AddImage(im->GetCV(600), *settings_.get());

    }
    catch (...) {
      emit Finished();
      return false;
    }
    images_.pop_front();
  }
  emit Finished();
  return true;
}

bool
HDRFuse::WasSuccessful() {
  while (!successes_.empty()) {
    if (successes_.begin()->get()) {
      successes_.pop_front();
    }
    else {
      successes_.pop_front();
      return false;
    }
  }
  return true;
}

void
HDRFuse::SetSettings(FuseSettings* settings) {
  *settings_.get() = *settings;
}

int
HDRFuse::GetNoImages() const {
  return fuser_->GetNoImages();
}

QImage
HDRFuse::GetFused() {
  std::lock_guard<std::mutex> lock(images_mutex_);
  assert(fuser_->GetNoImages() > 0);
  cv::Mat fusedimage;
  fuser_->GetFused(fusedimage, *settings_.get());
  QImage image = ImageUtil::Mat2QImage(fusedimage);
  emit FinishedPreview();
  return image;
}

void
HDRFuse::RemoveImage(int index) {
  // Wait for potential async operation
  std::lock_guard<std::mutex> lock(images_mutex_);
  assert(fuser_->GetNoImages() > index);
  fuser_->RemoveImage(index);
}

void
HDRFuse::RemoveImages() {
  // Wait for potential async operation
  std::lock_guard<std::mutex> lock(images_mutex_);
  fuser_->RemoveImages();
}
