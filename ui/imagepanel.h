/*
 * =====================================================================================
 *
 *       Filename:  imagepanel.h
 *
 *    Description:  Header file for imagepanel class
 *
 *        Version:  1.0
 *        Created:  11/01/2011 08:38:09 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Petter Moe Kvalvaag (pmk), petter@pkvalvaag.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef IMAGEPANEL_H
#define IMAGEPANEL_H

#include <QWidget>
#include <util/subject.h>
#include <util/reason.h>
#include <util/updateinfo.h>
#include <memory>

#include <ui_export.h>

class QPushButton;
class QLabel;
class QScrollArea;
class QScrollBar;
class ImageLabel;
class ImageProxy;
class HDR;

/*ScrollInfo is a struct that can be passed in update events, to 
provide information about direction and length of scrolling*/
struct UI_EXPORT ImageChangedInfo : public UpdateInfo {
  enum Direction{ HORIZONTAL, VERTICAL, BOTH };
  ImageChangedInfo() {}
  QString path_;
  int index_;
  Direction direction_;
  int length_;
  std::vector<int> lengths_; // use for updating both scroll directions
                             // in order horizontal, vertical
  bool zoomed_;
};

class UI_EXPORT ImagePanel : public QWidget, public Subject
{   
  Q_OBJECT

public:
  ImagePanel( QWidget *parent = 0 );

	~ImagePanel();

  void UpdateWidth(int width);
  void SetPath(QString path) { path_ = path; }
	QString GetImageFileName();

  static Reason ImageChangedReason() 
      { return Reason("ImageChangedReason"); }
  static Reason ImageScrolledReason() 
      { return Reason("ImageScrolledReason"); }
  static Reason ImageZoomedReason() 
      { return Reason("ImageZoomedReason"); }
  
  void MoveHorizontalSlider(int length);
  void MoveVerticalSlider(int length);
  void SetIndex(int index) { index_ = index; }
  int  GetIndex() { return index_; }
  ImageProxy* GetImage();
	bool OpenFileFromPath(const QString& path);

signals:
  void DeleteMe(int index);

public slots:
  bool OpenFile();
  void SlotZoom();
  void SlotRemove();

public:
  void Zoom();
  void ZoomIn();
  void ZoomOut();
private:
  void HorizontalSliderMoved(int position);
  void VerticalSliderMoved(int position);

  bool         zoom_;
  int          index_;
  QString      path_;
  ImageLabel  *imageArea_;
  QPushButton *openButton_;
  QScrollArea *scrollArea_;
  std::unique_ptr<ImageProxy> image_;
};

#endif


