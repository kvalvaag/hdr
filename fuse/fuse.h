#pragma once

#include <vector>

#include <fuse_export.h>

namespace cv {
  class Mat;
}

#ifdef fuse_EXPORTS
#    define EXPIMP_TEMPLATE
#else
#    define EXPIMP_TEMPLATE extern
#endif

// REF: https://support.microsoft.com/en-us/kb/168958
// Need this bit to export STL container members
// Instantiate classes vector<int> and vector<char>
// This does not create an object. It only forces the generation of all
// of the members of classes vector<int> and vector<char>. It exports
// them from the DLL and imports them into the .exe file.
EXPIMP_TEMPLATE template class FUSE_EXPORT std::vector<cv::Mat>;

struct FUSE_EXPORT FuseSettings {
FuseSettings() : sigma_(0.2f),
                 nopyramids_(4),
                 contrastweight_(1),
                 saturationweight_(1),
                 exposednessweight_(1),
                 horizontalresolution_(800) {}
  float sigma_;
  int nopyramids_;
  float contrastweight_;
  float saturationweight_;
  float exposednessweight_;
  int horizontalresolution_;
};

class FUSE_EXPORT Fuse
{
public:
  Fuse();
  ~Fuse();

  void AddImage(cv::Mat & image, FuseSettings settings);
  void RemoveImage(int index);
  void RemoveImages();

  void GetFused(cv::Mat& fused, FuseSettings settings);

  int GetNoImages();

private:
  std::vector<cv::Mat> float_images_;
  std::vector<cv::Mat> all_weights_;
};

