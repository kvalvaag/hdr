/*
 * observer.cpp
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#include <util/observer.h>

Observer::Observer() : notifying_(false) {

}

Observer::~Observer() {
}

void
Observer::Update(Reason reason, UpdateInfo* info) {
  if (!notifying_) {
    notifying_ = true;
    DoUpdate(reason, info);
    notifying_ = false;
  }
}

