/*
 * Preview.h
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#ifndef PREVIEW_H_
#define PREVIEW_H_

#include <QWidget>

#include <ui_export.h>

class ImageLabel;

class UI_EXPORT Preview : public QWidget{
  Q_OBJECT
public:

  Preview(QWidget *parent);
  virtual
  ~Preview();

  void SetImage(QImage image);

private:
  ImageLabel* imagelabel_;
};

#endif /* PREVIEW_H_ */
