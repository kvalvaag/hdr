/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Qt implementation of HDR program
 *
 *        Version:  1.0
 *        Created:  10/30/2011 02:34:29 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Petter Moe Kvalvaag (pmk), petter@pkvalvaag.com
 *        Company:  
 *
 * =====================================================================================
 */

#include <ui/HDR.h>
#include <QApplication>

// Include header files for application components.

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    HDR HDRapp;
    HDRapp.resize(600,400);
    HDRapp.show();
    return app.exec();
}

