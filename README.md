# HDR - Creating High Dynamic Range Pictures
HDR is a software written to generate HDR pictures in a simplistic manner. 

#Issue tracker
Report and browse at [http://hdr.myjetbrains.com/youtrack/issues](http://hdr.myjetbrains.com/youtrack/issues)

# Dependencies
Boost

Qt

OpenCV

# Building instructions
## Visual Studio
Download MS Visual Studio and get the latest Windows SDK
### Boost
Download boost and extract to $BOOST_FOLDER of your choice

Environment variables: 

BOOST_ROOT=$BOOST_FOLDER

### OpenCV
Download opencv and extract to $OPENCV_FOLDER

Environment variables:

OPENCV_DIR=$OPENCV_FOLDER\build

CMAKE_PREFIX_PATH+=$OPENCV_FOLDER\build\x64\vc12\bin

PATH+=OPENCV_FOLDER\build\x64\vc12\bin

### Qt5
Download Qt5 and instruct the installer to install qt at $QT_FOLDER

Environment variables:

QT_DIR=$QT_FOLDER\<version number>

CMAKE_PREFIX_PATH+=$QT_DIR\msvc2013_64\lib

PATH+=$QT_DIR\msvc2013_64\bin

###CMake

Download cmake

Run cmake-gui

Where is the source code: Points to this repository

Where to build the binaries: The filesystem location of the Visual Studio project you would like to use.

Press Configure and select the Visual Studio 12 2013 Win64 generator

Press Generate

### Creating Visual Studio solution
Open Visual Studio and open the solution that has been generated. In the solution explorer, set the HDR project as the StartUp project. You can now build, modify and run the software.

## Linux
### Dependencies:
opencv

opencv-devel

boost-devel

qt5

qt5-devel

If you distro does not include Qt5, download the installer from Digia and install to a custom location QT_FOLDER.

Environment variables:

CMAKE_PREFIX_PATH=$QT_FOLDER/<version number>/gcc64/lib/cmake

CMake enabled IDEs (QtCreator, CLion, etc.) should now be able to open this project by pointing to the CMakeLists.txt file at the root of this repository.