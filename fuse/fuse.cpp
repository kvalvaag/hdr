#include "fuse.h"

#include <opencv2/opencv.hpp>
#include <assert.h>

Fuse::Fuse()
{
}


Fuse::~Fuse()
{
}

void
Fuse::AddImage(cv::Mat & image, FuseSettings settings) {
  typedef cv::MatConstIterator_<cv::Vec3f> PixelIter;
  typedef cv::MatIterator_<float> BWPixelIter;
  cv::Mat source = cv::Mat(image.size(), CV_32FC3);
  image.convertTo(source, CV_32FC3, 1.0 / 255.0);
  float_images_.push_back(source);
  cv::Mat source_bw = cv::Mat(source.size(), CV_32FC1);
  cv::cvtColor(source, source_bw, CV_RGB2GRAY);
  cv::Mat contrastdest = cv::Mat(source.rows, source.cols, CV_32FC1);
  cv::Laplacian(source_bw, contrastdest, CV_32F, 5);
  contrastdest = cv::abs(contrastdest);

  cv::Mat saturation = cv::Mat(source.rows, source.cols, CV_32FC1);
  cv::Mat exposedness = cv::Mat(source.rows, source.cols, CV_32FC1);
  cv::Mat weights = cv::Mat(source.rows, source.cols, CV_32FC1);

  BWPixelIter contrastiter = contrastdest.begin<float>();
  BWPixelIter saturationiter = saturation.begin<float>();
  BWPixelIter exposednessiter = exposedness.begin<float>();
  BWPixelIter weightsiter = weights.begin<float>();

  for (PixelIter pixel = source.begin<cv::Vec3f>();
    pixel != source.end<cv::Vec3f>(); ++pixel) {
    float mean = ((*pixel)[0] * (*pixel)[0] +
      (*pixel)[1] * (*pixel)[1] +
      (*pixel)[2] * (*pixel)[2]) / 3;
    float stddev = sqrt(((*pixel)[0] - mean) *
      ((*pixel)[0] - mean) +
      ((*pixel)[1] - mean) * ((*pixel)[1] - mean) +
      ((*pixel)[2] - mean) * ((*pixel)[2] - mean));
    *saturationiter = stddev;

    std::vector<float> expose;
    for (int i = 0; i < 3; ++i) {
      expose.push_back(exp(-(((*pixel)[i] - 0.5f) * ((*pixel)[i] - 0.5f)) /
        (2.0f * pow(settings.sigma_, 2))));
    }
    *exposednessiter = expose.at(0) * expose.at(1) * expose.at(2);
    float wi = pow(*contrastiter, settings.contrastweight_) *
      pow(*saturationiter, settings.saturationweight_) *
      pow(*exposednessiter, settings.exposednessweight_);
    *weightsiter = wi;

    ++saturationiter;
    ++exposednessiter;
    ++contrastiter;
    ++weightsiter;
  }
  all_weights_.push_back(weights);
}



void
Fuse::RemoveImage(int index) {
  assert(float_images_.size() > index);
  float_images_.erase(float_images_.begin() + index);
  assert(all_weights_.size() > index);
  all_weights_.erase(all_weights_.begin() + index);
}

void
Fuse::RemoveImages() {
  float_images_.clear();
  all_weights_.clear();
}

void
Fuse::GetFused(cv::Mat& fused, FuseSettings settings) {
  
  size_t no_images = float_images_.size();
  assert(no_images > 0);
  
  std::vector<cv::Mat> all_weights_normal;
  //std::vector<cv::Mat> float_images;
  
  for (cv::Mat weight : all_weights_) {
    cv::Mat tmp;
    weight.copyTo(tmp);
    all_weights_normal.push_back(tmp);
  }
  // Normalize weights
  for (size_t i = 0; i < all_weights_normal[0].rows; ++i) {
    for (size_t j = 0; j < all_weights_normal[0].cols; ++j) {
      float sum = 0;
      for (size_t k = 0; k < all_weights_normal.size(); ++k) {
        sum += all_weights_normal.at(k).at<float>(int(i), int(j));
      }
      if (sum <= 0.0f) { // No images have a good measure,
                         // choose the middle image arbitrarily
        all_weights_normal.at(int(all_weights_normal.size()) / 2).
          at<float>(int(i), int(j)) = 1.0;
      }
      else {
        for (size_t k = 0; k < all_weights_normal.size(); ++k) {
          all_weights_normal.at(k).at<float>(int(i), int(j)) /= sum;
        }
      }
    }
  }

  // Naive fusion
  /*cv::Mat float_fused = cv::Mat::zeros(images_.at(0).size(), CV_32FC3);
  for (size_t i = 0; i < no_images; ++i) {
    cv::Mat contrib = cv::Mat(images_.at(0).size(), CV_32FC3);
    cv::cvtColor(all_weights[i], contrib, CV_GRAY2RGB);
    float_fused += contrib.mul(float_images[i]);
  }*/

  //Pyramid fusion
  cv::Mat float_fused = 
    cv::Mat::zeros(float_images_.at(0).size(), CV_32FC3);
  cv::Mat tmp, tmp2, tmp3;
  for (size_t i = 0; i < no_images; ++i) {
    std::vector<cv::Mat> laplace_images, gauss_weights; 
    cv::Mat tmp, tmp2;
    all_weights_normal[i].copyTo(tmp);
    gauss_weights.push_back(all_weights_normal[i]);
    std::vector<cv::Size> sizes;
    //Gaussian pyramid of weights, and track sizes of pyramid images
    for (int j = 1; j < settings.nopyramids_; ++j) {
      sizes.push_back(tmp.size());
      cv::pyrDown(tmp, tmp2);
      gauss_weights.push_back(tmp2);
      tmp = tmp2;
    }
    
    //Laplacian pyramids of images
    cv::Mat tmp3;
    float_images_[i].copyTo(tmp);
    for (int j = 0; j < (settings.nopyramids_ - 1); ++j) {
      cv::pyrDown(tmp, tmp2);
      cv::pyrUp(tmp2, tmp3, tmp.size());
      laplace_images.push_back(tmp - tmp3);
      tmp = tmp2;
    }
    laplace_images.push_back(tmp2);
    //Multiply images and weights at each level
    for (int j = 0; j < settings.nopyramids_; ++j) {
      cv::Mat weightmap;
      cv::cvtColor(gauss_weights[j], weightmap, CV_GRAY2RGB);
      laplace_images[j] = laplace_images[j].mul(weightmap);
    }

    //Build contribution
    laplace_images[settings.nopyramids_ - 1].copyTo(tmp);
    for (int j = settings.nopyramids_ - 2; j >= 0; --j) {
      cv::pyrUp(tmp, tmp, sizes[j]);
      tmp += laplace_images[j];
    }
    float_fused += tmp;
  }
  
  float_fused.convertTo(fused, CV_8UC3, 255.0);
}

int
Fuse::GetNoImages() {
  return static_cast<int>(float_images_.size());
}