#pragma once

#include <util_export.h>

namespace cv {
  class Mat;
}

class QImage;

namespace ImageUtil
{
  UTIL_EXPORT cv::Mat QImage2Mat(const QImage& qimage);
  UTIL_EXPORT QImage Mat2QImage(const cv::Mat& mat);
};
