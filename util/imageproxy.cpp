/*
 * ImageProxy.cpp
 *
 *  Created on: Nov 17, 2012
 *      Author: petter
 */

#include "imageproxy.h"

#include <util/imageutil.h>

#include <QImage>

#include <opencv2/opencv.hpp>


/*!
 * Proxy class which keeps image filename and
 * provides scaled copies
 */
ImageProxy::ImageProxy() :
  fullsize_(std::make_unique<QImage>()),
  thumb_(std::make_unique<QImage>()),
  cvfullsize_(std::make_unique<cv::Mat>()),
  cvthumb_(std::make_unique<cv::Mat>()) {
}

ImageProxy::ImageProxy(QString filename) :
  filename_(filename),
  fullsize_(std::make_unique<QImage>()),
  thumb_(std::make_unique<QImage>()),
  cvfullsize_(std::make_unique<cv::Mat>()),
  cvthumb_(std::make_unique<cv::Mat>()) {
}

ImageProxy::~ImageProxy() {
}

QImage*
ImageProxy::GetImage() {
  if (fullsize_->isNull() && !filename_.isEmpty()) {
    cv::Mat tmp = cv::imread(filename_.toLocal8Bit().constData(), 3);
    *fullsize_ = ImageUtil::Mat2QImage(tmp);
  }
  return fullsize_.get();
}

cv::Mat
ImageProxy::GetCV(int width) {
  return ImageUtil::QImage2Mat(*GetThumb(width));
}

cv::Mat ImageProxy::GetCV() {
  return ImageUtil::QImage2Mat(*fullsize_);
}

QString ImageProxy::GetFileName()
{
	return filename_;
}

QImage*
ImageProxy::GetThumb() {
  if (thumb_->isNull()) {
    return GetThumb(200);
  }
  else {
    return thumb_.get();
  }
}

QImage*
ImageProxy::GetThumb(int width) {

  if (width == current_thumb_width_ &&
      !thumb_->isNull()) {
    return thumb_.get();
  }

  QImage *image = GetImage();
  if (!image->isNull()) {
    if (image->width() / image->height()) {
      *thumb_ = image->scaledToWidth(width);
    }
    else {
      *thumb_ = image->scaledToWidth(width);
    }
    current_thumb_width_ = width;
  }
  return thumb_.get();
}

double
ImageProxy::GetRatio() {
  return (static_cast<double>(thumb_->width()) /
      static_cast<double>(thumb_->height()));
}

int
ImageProxy::GetHeight() {
  return fullsize_->height();
}

int
ImageProxy::GetWidth() {
  return fullsize_->width();
}

bool
ImageProxy::isNull() {
  return fullsize_->isNull();
}
