//
// Created by petter on 6/15/15.
// Any compiler support additions go here

#ifndef HDR_COMPATILBILITY_H
#define HDR_COMPATILBILITY_H

#ifndef _WINDOWS
/* GCC 4.8 doesn't have std::make_unique from C++14, so add that here */
namespace std {
  template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args)
  {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }
}
#endif

#endif //HDR_COMPATILBILITY_H
