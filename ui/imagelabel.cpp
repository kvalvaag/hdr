/*
 * imagelabel.cpp
 *
 *  Created on: Nov 18, 2012
 *      Author: petter
 */

#include "ui/imagelabel.h"
#include <QMouseEvent>

ImageLabel::ImageLabel(QWidget * parent) : QLabel(parent) {
}

ImageLabel::~ImageLabel()
{
}

void ImageLabel::mouseDoubleClickEvent(QMouseEvent* e)
{
  if (e->button() == Qt::LeftButton) {
    x_ = e->x();
    y_ = e->y();
    emit doubleClicked();
  }
}
