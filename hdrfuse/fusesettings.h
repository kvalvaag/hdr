#pragma once

#include <memory>

#include <util/reason.h>
#include <util/subject.h>

#include <QWidget>

#include <hdrfuse_export.h>

struct FuseSettings;
class HDRFuse;
class QJsonObject;

namespace Ui {
  class FuseSettings;
}

class HDRFUSE_EXPORT FuseSettingsUi : public Subject, public QWidget {
public:
  FuseSettingsUi(QWidget* parent = nullptr);
  virtual ~FuseSettingsUi();

  static Reason ImageSettingsChangedReason()
  {
    return Reason("ImageSettingsChangedReason");
  }
  static Reason FuseSettingsChangedReason()
  {
    return Reason("FuseSettingsChangedReason");
  }

  void GetFuseSettings(HDRFuse* hdrfuse);
	void Read(const QJsonObject& json_object);
	void Write(QJsonObject& json_object);

private:
  std::unique_ptr<Ui::FuseSettings> ui_;
  std::unique_ptr<FuseSettings> settings_;

private slots:
  void SigmaChanged(double sigma);
  void NoPyramidsChanged(int no_pyramids);
  void ContrastWeightChanged(double contrastweight);
  void SaturationWeightChanged(double saturationweight);
  void ExposednessWeightChanged(double exposednessweight);
};