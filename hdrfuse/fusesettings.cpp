
#include <fusesettings.h>
#include <fuse/fuse.h>

#include <util/compatibility.h>

#include <hdrfuse/hdrfuse.h>

#include <ui_fusesettings.h>

#include <QtCore/QJsonObject>

FuseSettingsUi::FuseSettingsUi(QWidget *parent) : QWidget(parent),
                                              ui_(std::make_unique<Ui::FuseSettings>()),
                                              settings_(std::make_unique<FuseSettings>())
{
  ui_->setupUi(this);
  connect(ui_->sigma_, static_cast<void (QDoubleSpinBox::*)(const double)>
    (&QDoubleSpinBox::valueChanged),
    this, &FuseSettingsUi::SigmaChanged);
  connect(ui_->no_pyramids_, static_cast<void (QSpinBox::*)(const int)>
    (&QSpinBox::valueChanged),
    this, &FuseSettingsUi::NoPyramidsChanged);
  connect(ui_->contrast_weight_, static_cast<void (QDoubleSpinBox::*)(const double)>
    (&QDoubleSpinBox::valueChanged),
    this, &FuseSettingsUi::ContrastWeightChanged);
  connect(ui_->saturation_weight, static_cast<void (QDoubleSpinBox::*)(const double)>
    (&QDoubleSpinBox::valueChanged),
    this, &FuseSettingsUi::SaturationWeightChanged);
  connect(ui_->exposedness_weight, static_cast<void (QDoubleSpinBox::*)(const double)>
    (&QDoubleSpinBox::valueChanged),
    this, &FuseSettingsUi::ExposednessWeightChanged);
}

void
FuseSettingsUi::GetFuseSettings(HDRFuse* hdrfuse) {
  hdrfuse->SetSettings(settings_.get());
}

void FuseSettingsUi::Read(const QJsonObject& json_object)
{
	ui_->sigma_->setValue(json_object["sigma"].toDouble());
	ui_->no_pyramids_->setValue(json_object["no_pyramids"].toInt());
	ui_->contrast_weight_->setValue(json_object["contrast_weight"].toDouble());
	ui_->saturation_weight->setValue(
		json_object["saturation_weight"].toDouble());
	ui_->exposedness_weight->setValue(
		json_object["exposedness_weight"].toDouble());
}

void FuseSettingsUi::Write(QJsonObject& json_object)
{
	json_object["sigma"] = ui_->sigma_->value();
	json_object["no_pyramids"] = ui_->no_pyramids_->value();
	json_object["contrast_weight"] = ui_->contrast_weight_->value();
	json_object["saturation_weight"] = ui_->saturation_weight->value();
	json_object["exposedness_weight"] = ui_->exposedness_weight->value();
}

FuseSettingsUi::~FuseSettingsUi() {
}

void
FuseSettingsUi::SigmaChanged(double sigma) {
  settings_->sigma_ = sigma;
  Notify(ImageSettingsChangedReason());
}

void
FuseSettingsUi::NoPyramidsChanged(int no_pyramids) {
  settings_->nopyramids_ = no_pyramids;
  Notify(FuseSettingsChangedReason());
}

void
FuseSettingsUi::ContrastWeightChanged(double contrastweight) {
  settings_->contrastweight_ = contrastweight;
  Notify(ImageSettingsChangedReason());
}

void
FuseSettingsUi::SaturationWeightChanged(double saturationweight) {
  settings_->saturationweight_= saturationweight;
  Notify(ImageSettingsChangedReason());
}

void
FuseSettingsUi::ExposednessWeightChanged(double exposednessweight) {
  settings_->exposednessweight_ = exposednessweight;
  Notify(ImageSettingsChangedReason());
}