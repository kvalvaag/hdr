/*
 * observer.h
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#ifndef OBSERVER_H_
#define OBSERVER_H_

#include <util/reason.h>
#include <util/updateinfo.h>

#include <util_export.h>

class UTIL_EXPORT Observer {
public:
  Observer();
  virtual ~Observer();

  void         Update(Reason reason, UpdateInfo* info = 0);
  virtual void DoUpdate(Reason reason, UpdateInfo* info) = 0;

private:
  bool notifying_;
};

#endif /* OBSERVER_H_ */
