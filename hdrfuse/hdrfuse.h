#pragma once

#include <memory>
#include <mutex>
#include <future>
#include <deque>
#include <QtCore/QObject>

#include <hdrfuse_export.h>

struct FuseSettings;
class Fuse;
class ImageProxy;
class QImage;

class HDRFUSE_EXPORT HDRFuse : public QObject
{
  Q_OBJECT

public:
  explicit HDRFuse(QObject *parent);
  virtual ~HDRFuse();

  void     AddImage(ImageProxy* image);
  int      GetNoImages() const;

  QImage   GetFused();
  void     RemoveImage(int index);
  void     RemoveImages();
  bool     ProcessImages();

  bool     WasSuccessful();

  void     SetSettings(FuseSettings* settings);

signals:
  void     Finished();
  void     FinishedPreview();

private:
  std::unique_ptr<Fuse>         fuser_;
  std::unique_ptr<FuseSettings> settings_;
  std::deque<ImageProxy*>       images_;
  std::deque<std::future<bool>> successes_;
  std::mutex                    images_mutex_;
};

