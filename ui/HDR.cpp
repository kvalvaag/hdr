/*
 * =====================================================================================
 *
 *       Filename:  HDR.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/30/2011 02:40:24 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Petter Moe Kvalvaag (pmk), petter@pkvalvaag.com
 *        Company:  
 *
 * =====================================================================================
 */
#include "HDR.h"

#include "ui/preview.h"

#include <util/imageproxy.h>
#include <util/compatibility.h>

#include <hdrfuse/hdrfuse.h>
#include <hdrfuse/fusesettings.h>

#include <QAction>
#include <QMenu>
#include <QPushButton>
#include <QScrollArea>
#include <QtGui>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMenuBar>
#include <QtCore/QJsonObject>
#include <QFileDialog>

// Initial width of left panel
static int INIT_LEFT_POSITION = 250; 
// Margin between image in left panel and splitter
static int RIGHT_IMAGE_MARGIN = 40;  

HDR::HDR()
  //TODO initialize all
  : QMainWindow(),
    fuser_(std::make_unique<HDRFuse>(this)),
		fileMenu_(nullptr),
		newAct_(nullptr),
		openAct_(nullptr),
		saveAct_(nullptr),
		printAct_(nullptr),
		exitAct_(nullptr),
	pos_(INIT_LEFT_POSITION)
{
  MakePanel();
	MakeActions();
	MakeMenu();
  ConnectSignalsAndSlots();
}

HDR::~HDR() {}

void
HDR::MakePanel() {
  //TODO: Split main widget out of HDR class
  mainWidget = new QWidget(this);
  left_panel_ = new QWidget(mainWidget);
  left_panel_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  left_panel_scroll_ = new QScrollArea(mainWidget);
  left_panel_scroll_->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  left_panel_scroll_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  right_panel_ = new QTabWidget(mainWidget);
  fusesettings_ = new FuseSettingsUi(this);
  right_panel_->addTab(fusesettings_, "Settings");
  fusesettings_->Attach(this);
  preview_ = new Preview(mainWidget); 
  right_panel_->addTab(preview_, tr("Preview"));
  QLabel *titleLabel = new QLabel( tr( "<b>Choose source images</b>" ), mainWidget );

  splitter_ = new QSplitter(mainWidget);

  buttonsLayout_ = new QHBoxLayout();
  leftLayout_    = new QVBoxLayout();
  leftLayout_->setSizeConstraint(QLayout::SetMinimumSize);
  plusButton_    = new QPushButton("+", mainWidget);
  minusButton_   = new QPushButton("-", mainWidget);

  buttonsLayout_->addWidget(minusButton_);
  buttonsLayout_->addWidget(plusButton_);
  buttonsLayout_->setAlignment(Qt::AlignLeft);

  leftLayout_->addWidget(titleLabel);
  leftLayout_->addLayout(buttonsLayout_);
  for (uint i = 0; i < images_.size(); ++i) {
    leftLayout_->addWidget( images_[i].get() );
  }

  left_panel_->setLayout(leftLayout_);
  left_panel_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  left_panel_scroll_->setWidget(left_panel_);
  splitter_->addWidget(left_panel_scroll_);
  splitter_->addWidget(right_panel_);
  splitter_->setStretchFactor(1,1);

  setCentralWidget(splitter_);
  setWindowTitle( tr( "HDRapp" ) );
     
  QList<int> sizes;
  sizes.append(INIT_LEFT_POSITION);
  sizes.append(400);
  splitter_->setSizes(sizes);
  SplitterMoved(INIT_LEFT_POSITION);
  left_panel_->adjustSize();
}

void
HDR::MakeActions() {
  newAct_ = new QAction(tr("&New"), this);
  openAct_ = new QAction(tr("&Open"), this);
  saveAct_ = new QAction(tr("&Save"), this);
	testAct_ = new QAction(tr("&Test"), this);
  printAct_ = new QAction(tr("&Print"), this);
  exitAct_ = new QAction(tr("&Exit"), this);
	connect(exitAct_, &QAction::triggered,
		this, &HDR::close);
	connect(openAct_, &QAction::triggered,
		this, &HDR::LoadProject);
	connect(saveAct_, &QAction::triggered,
		this, &HDR::SaveProject);
}

void
HDR::MakeMenu() {
	menuBar()->setEnabled(true);
	menuBar()->setNativeMenuBar(false);
  fileMenu_ = menuBar()->addMenu(tr("&File"));
  fileMenu_->addAction(newAct_);
  fileMenu_->addAction(openAct_);
  fileMenu_->addAction(saveAct_);
	fileMenu_->addAction(testAct_);
  fileMenu_->addAction(printAct_);
  fileMenu_->addSeparator();
	fileMenu_->addAction(exitAct_);

  /*editMenu = menuBar()->addMenu(tr("&Edit"));
  editMenu->addAction(undoAct);
  editMenu->addAction(redoAct);
  editMenu->addSeparator();
  editMenu->addAction(cutAct);
  editMenu->addAction(copyAct);
  editMenu->addAction(pasteAct);
  editMenu->addSeparator();

  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct);*/
}

void
HDR::ConnectSignalsAndSlots() {
  connect(plusButton_, &QPushButton::clicked , this, &HDR::AddImage);
  connect(minusButton_, &QPushButton::clicked, this, &HDR::RemoveImage);
  connect(splitter_, &QSplitter::splitterMoved,
      this, &HDR::SplitterMoved);
}

void
HDR::DoUpdate(Reason reason, UpdateInfo* info) {
  if (reason == ImagePanel::ImageChangedReason() &&
      info != 0) {
    ImageChangedInfo* imageinfo =
        dynamic_cast<ImageChangedInfo*>(info);
    if (imageinfo != 0) {
      UpdatePaths(imageinfo->path_);
      int index = imageinfo->index_;
      ImageProxy* image = images_.at(index)->GetImage();
      connect(fuser_.get(), &HDRFuse::Finished,
        this, &HDR::UpdatePreview);
      fuser_->AddImage(image);
    }
    SplitterMoved(pos_);
    return;
  }
  if (reason == ImagePanel::ImageScrolledReason()) {
    ImageChangedInfo* imageinfo =
        dynamic_cast<ImageChangedInfo*>(info);
    if (imageinfo != 0)
      UpdateScrollAreas(imageinfo);
    return;
  }
  if (reason == ImagePanel::ImageZoomedReason() &&
      info != 0) {
    ImageChangedInfo* imageinfo =
        dynamic_cast<ImageChangedInfo*>(info);
    if (imageinfo != nullptr) {
      UpdateZoomStatus(imageinfo);
    }
    return;
  }
  if (reason == FuseSettingsUi::ImageSettingsChangedReason()) {
    InvalidateImages();
    return;
  }
  if (reason == FuseSettingsUi::FuseSettingsChangedReason()) {
    InvalidatePreview();
    return;
  }
}

void
HDR::UpdatePaths(QString path) {
	for (Image img : images_) {
		img->SetPath(path);
	}
}

void
HDR::AddImage() {
  Image imagePanel(new ImagePanel());
  imagePanel->SetIndex(int(images_.size()));
  imagePanel->Attach(this);
  leftLayout_->addWidget(imagePanel.get());
  images_.push_back(imagePanel);
	if (imagePanel->OpenFile())
	{
    leftLayout_->removeWidget(imagePanel.get());
    left_panel_->adjustSize();
    images_.pop_back();
    return;
	}
  imagePanel->UpdateWidth(pos_ - RIGHT_IMAGE_MARGIN);
  left_panel_->adjustSize();
  connect(imagePanel.get(), &ImagePanel::DeleteMe,
    this, &HDR::RemoveImageNo);
}

void
HDR::RemoveImage() {
	if (images_.size() < 1) {
		return;
	}

  fuser_->RemoveImage(images_[images_.size() -1]->GetIndex());
  images_.pop_back();
  UpdatePreview();
  left_panel_->adjustSize();
}

void
HDR::RemoveImageNo(int index) {
	//assert(images_.size() > index);
  fuser_->RemoveImage(index);
  images_.erase(images_.begin() + index);
  UpdateIndices();
  UpdatePreview();
  left_panel_->adjustSize();
}

void
HDR::InvalidateImages() {
  fuser_->RemoveImages();
  fusesettings_->GetFuseSettings(fuser_.get());
  connect(fuser_.get(), &HDRFuse::Finished,
    this, &HDR::UpdatePreview);
  for (Image img : images_) {
    fuser_->AddImage(img->GetImage());
  }
}

void HDR::CreateFullSizeFused(const QString& filename) {
  HDRFuse fuser(this);
  for (Image img : images_) {
    fuser.AddImage(img->GetImage());
  }

}

void
HDR::InvalidatePreview() {
  fusesettings_->GetFuseSettings(fuser_.get());
  UpdatePreview();
}

void
HDR::UpdateIndices() {
  for (size_t i = 0; i < images_.size(); ++i) {
    images_[i]->SetIndex(int(i));
  }
}

void HDR::Read(const QJsonObject& json_object)
{
	while (images_.size() > 0)
	{
		RemoveImage();
	}
	uint count = 0;
	QJsonArray files = json_object["files"].toArray();
	QJsonObject file;

	for (QJsonArray::const_iterator iter = files.constBegin();
		iter != files.constEnd(); ++iter)
	{
		auto imagePanel = std::make_shared<ImagePanel>();
		leftLayout_->addWidget(imagePanel.get());
		images_.push_back(imagePanel);
		imagePanel->SetIndex(int(images_.size()) - 1);
		imagePanel->Attach(this);
		imagePanel->UpdateWidth(pos_ - RIGHT_IMAGE_MARGIN);
		imagePanel->OpenFileFromPath((*iter).toString());
		left_panel_->adjustSize();
		connect(imagePanel.get(), &ImagePanel::DeleteMe,
			this, &HDR::RemoveImageNo);
	}
	const QJsonObject fusesettings = json_object["settings"].toObject();
	fusesettings_->Read(fusesettings);
}

void HDR::Write(QJsonObject& json_object)
{
	QJsonArray files;
	for (Image img : images_)
	{
		QJsonValue file = img->GetImageFileName();
		files.append(file);
	}
	json_object["files"] = files;
	QJsonObject fuse_settings;
	fusesettings_->Write(fuse_settings);
	json_object["settings"] = fuse_settings;
}

void HDR::Load(const QString& filename)
{
	QFile loadfile(filename);
	if (!loadfile.open(QIODevice::ReadOnly))
	{
		//TODO message
		//qWarning("Could not load file " + filename);
	}
	QByteArray save_data = loadfile.readAll();

	QJsonDocument load_document(QJsonDocument::fromJson(save_data));
	Read(load_document.object());
}

void HDR::Save(const QString& filename)
{
	QFile savefile(filename);
	if (!savefile.open(QIODevice::WriteOnly))
	{
		//TODO message
	}
	QJsonObject save_object;
	Write(save_object);
	QJsonDocument save_document(save_object);
	savefile.write(save_document.toJson());
}

void
HDR::SplitterMoved(int pos) {
	pos_ = pos;
  for(Image img : images_) {
    int left = 0; int right = 0; int top = 0; int bottom = 0;
    left_panel_scroll_->getContentsMargins(&left, &top, &right, &bottom);
    img->UpdateWidth(pos - RIGHT_IMAGE_MARGIN);
  }
  left_panel_->adjustSize();
}

void HDR::LoadProject()
{
	const QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open File"), "", "*.hdr");
	Load(fileName);
}

void HDR::SaveProject()
{
	const QString fileName = QFileDialog::getSaveFileName(this,
		tr("Save File"), "", "*.hdr");
	Save(fileName);
}


void
HDR::UpdateScrollAreas(ImageChangedInfo* info) {
  for (Image img : images_) {
    if (info->direction_ == ImageChangedInfo::HORIZONTAL) {
        img->MoveHorizontalSlider(info->length_);
    }
    else {
      img->MoveVerticalSlider(info->length_);
    }
  }
}

void
HDR::UpdateZoomStatus(ImageChangedInfo *info) {
  for (Image img : images_) {
    if (info->zoomed_) {
      img->ZoomIn();
      img->MoveHorizontalSlider(info->lengths_[0]);
      img->MoveVerticalSlider(info->lengths_[1]);
    }
    else {
      img->ZoomOut();
    }
  }
}

void
HDR::UpdatePreview() {
  if (fuser_->GetNoImages() > 0 && fuser_->WasSuccessful()) {
    QEventLoop q;
    connect(fuser_.get(), &HDRFuse::FinishedPreview,
            &q, &QEventLoop::quit);
    auto image = std::async(std::launch::async, [] (HDRFuse* fuse) -> QImage
               {return fuse->GetFused(); }, fuser_.get());
    q.exec();
    QImage imagefut = image.get();
    preview_->SetImage(imagefut);
  }
}
