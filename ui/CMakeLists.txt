# QT
find_package(Qt5Widgets)
find_package(Qt5Core)

include(GenerateExportHeader)

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(SRCS
  CMakeLists.txt
  HDR.cpp
  HDR.h
  imagelabel.cpp
  imagelabel.h
  imagepanel.cpp
  imagepanel.h
  preview.cpp
  preview.h)
add_library(ui ${LIB_TYPE} ${SRCS})
if (NOT BUILD_SHARED_LIBS)
  target_compile_definitions(ui PUBLIC UI_STATIC_DEFINE)
endif()
target_link_libraries(ui util hdrfuse Qt5::Widgets Qt5::Core)
target_include_directories(ui PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
GENERATE_EXPORT_HEADER(ui EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/exports/ui_export.h)
install(TARGETS ui
	RUNTIME
	DESTINATION bin
  ARCHIVE DESTINATION lib
	COMPONENT libraries)
