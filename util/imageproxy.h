/*
 * ImageProxy.h
 *
 *  Created on: Nov 17, 2012
 *      Author: petter
 */

#ifndef IMAGEPROXY_H_
#define IMAGEPROXY_H_

#include <QtCore/QString>
#include <memory>

#include <util_export.h>

class QImage;

namespace cv {
  class Mat;
}

class UTIL_EXPORT ImageProxy {
public:
  ImageProxy();
  explicit ImageProxy(QString filename);
  ~ImageProxy();

  QImage*  GetImage();
  QImage*  GetThumb();
  QImage*  GetThumb(int width);
  cv::Mat  GetCV(int width);
  cv::Mat  GetCV();
	QString  GetFileName();

  double   GetRatio();
  int      GetWidth();
  int      GetHeight();

  bool     isNull();

private:
  QString  filename_;
  std::unique_ptr<QImage> fullsize_;
  std::unique_ptr<QImage> thumb_;
  std::unique_ptr<cv::Mat> cvfullsize_;
  std::unique_ptr<cv::Mat> cvthumb_;
  int      current_thumb_width_;
};

#endif /* IMAGEPROXY_H_ */
