#ifndef HDR_H
#define HDR_H

#include "ui/imagepanel.h"
#include <util/observer.h>
#include <QMainWindow>
#include <vector>
#include <memory>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qsplitter.h>

#include <ui_export.h>

class Preview;
class FuseSettingsUi;
class HDRFuse;
class QWidget;
class QGridLayout;
class QVBoxLayout;
class QHBoxLayout;
class QJsonObject;
class QScrollArea;
class QPushButton;
class QSplitter;
class QTabWidget;
class QMenu;
class QAction;
struct ImageChangedInfo;

class UI_EXPORT HDR : public QMainWindow, public Observer
{
  Q_OBJECT

  using Image = std::shared_ptr<ImagePanel>;
	using Images = std::vector<Image>;

public:
  HDR();
  virtual ~HDR();
  
private:
  void MakePanel();
  void MakeMenu();
  void MakeActions();
  void ConnectSignalsAndSlots();
  void UpdatePaths(QString path);
  void RemoveImageNo(int index);
  void InvalidateImages();
  void CreateFullSizeFused(const QString& filename);
  void InvalidatePreview();
  void UpdateIndices();
	void Read(const QJsonObject& json_object);
	void Write(QJsonObject& json_object);
	void Load(const QString& filename);
	void Save(const QString& filename);

  //Inherited from Observer
  virtual void DoUpdate(Reason reason, UpdateInfo* info) override;

  Preview        *preview_;
  FuseSettingsUi *fusesettings_;
  QWidget        *mainWidget;
  QWidget        *left_panel_;
  QTabWidget     *right_panel_;
  Images          images_;
  QHBoxLayout    *buttonsLayout_;
  QVBoxLayout    *leftLayout_;
  QPushButton    *plusButton_;
  QPushButton    *minusButton_;
  QScrollArea    *left_panel_scroll_;
  QSplitter      *splitter_;
  QMenu          *fileMenu_;

  QAction *newAct_;
  QAction *openAct_;
  QAction *saveAct_;
	QAction *testAct_;
  QAction *printAct_;
  QAction *exitAct_;

  int      pos_; // Splitter position when last moved
  std::unique_ptr<HDRFuse> fuser_;

private slots:
  void RemoveImage();
  void AddImage();
  void SplitterMoved(int pos);
	void LoadProject();
	void SaveProject();
  void UpdateScrollAreas(ImageChangedInfo* info);
  void UpdateZoomStatus(ImageChangedInfo* info);
  void UpdatePreview();
};

#endif

