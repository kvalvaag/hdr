/*
 * =====================================================================================
 *
 *       Filename:  imagepanel.cpp
 *
 *    Description:  Implements small imagepanel for HDR application
 *
 *        Version:  1.0
 *        Created:  11/01/2011 08:23:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Petter Moe Kvalvaag (pmk), petter@pkvalvaag.com
 *        Company:  
 *
 * =====================================================================================
 */


#include <QDataStream>
#include <QGridLayout>
#include <QImage>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <iostream>
#include "ui/imagepanel.h"
#include "ui/imagelabel.h"
#include "util/imageproxy.h"

namespace {
  /* CalculateScroll figures out how much an image of full size
  \param image_width x \param image_height should scroll when zoomed
  100% inside a scroll area sized \param scrollarea_width x
  \param scrollarea_height, and the zoom is requested to centre
  around the point \param pos_x and \param pos_y. Returns the number
  of pixels to scroll.
   */
  void
  CalculateScroll(int image_width, int image_height,
                  double pos_x, double pos_y,
                  double scrollarea_width, double scrollarea_height,
                  int& scroll_x, int& scroll_y) {

    double relative_x = pos_x / scrollarea_width;
    double relative_y = pos_y / scrollarea_height;

    double centre_x = image_width * relative_x;
    double centre_y = image_height * relative_y;

    scroll_x = int(std::max(centre_x - scrollarea_width / 2, 0.0));
    scroll_y = int(std::max(centre_y - scrollarea_height / 2, 0.0));
  }
}

ImagePanel::ImagePanel(QWidget* parent)
  : QWidget(parent),
    image_(std::make_unique<ImageProxy>()) {
  //setMinimumWidth(200);
  setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  imageArea_ = new ImageLabel(parent);
  //setMinimumSize(400,400);
  //setMaximumSize(2000,2000);
  imageArea_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  imageArea_->setBackgroundRole(QPalette::Dark);
  imageArea_->setText(tr("Click \"Open\" to open image"));

  openButton_ = new QPushButton("&Open", this);
  openButton_->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  connect(openButton_, &QPushButton::clicked, this, &ImagePanel::OpenFile);

  QPushButton* close_button = new QPushButton("Remove", this);
  close_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  connect(close_button, &QPushButton::clicked, this, &ImagePanel::SlotRemove);

  scrollArea_ = new QScrollArea(this);
  scrollArea_->setBackgroundRole(QPalette::Dark);
  scrollArea_->setWidget(imageArea_);

  QGridLayout* layout = new QGridLayout(this);
  layout->addWidget(scrollArea_, 0, 0, 1, 2, Qt::AlignLeft);
  layout->addWidget(openButton_, 1, 0, Qt::AlignLeft);
  layout->addWidget(close_button, 1, 1, Qt::AlignLeft);

  connect(imageArea_, &ImageLabel::doubleClicked,
          this, &ImagePanel::SlotZoom);

  QScrollBar* v_scroll = scrollArea_->verticalScrollBar();
  QScrollBar* h_scroll = scrollArea_->horizontalScrollBar();
  connect(v_scroll, &QScrollBar::sliderMoved,
          this, &ImagePanel::VerticalSliderMoved);
  connect(h_scroll, &QScrollBar::sliderMoved,
          this, &ImagePanel::HorizontalSliderMoved);

  setLayout(layout);
}

ImagePanel::~ImagePanel() {
}

bool ImagePanel::OpenFileFromPath(const QString& fileName) {
  path_ = fileName;
  if (!fileName.isEmpty()) {
    image_ = std::make_unique<ImageProxy>(fileName);
    if (image_->GetThumb()->isNull()) {
      QMessageBox::information(this, tr("Image Panel"),
                               tr("Cannot load %1").arg(fileName));
      return true;
    }

    scrollArea_->setFixedSize(image_->GetThumb()->size());
    imageArea_->setFixedSize(image_->GetThumb()->size() - QSize(2, 2));
    scrollArea_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    imageArea_->setPixmap(QPixmap::fromImage(*(image_->GetThumb())));
    imageArea_->setScaledContents(false);
    zoom_ = false;
    ImageChangedInfo info;
    info.index_ = index_;
    info.path_ = fileName;
    Notify(ImageChangedReason(), &info);
    return false;
  }
  return true;
}

bool ImagePanel::OpenFile() {
  const QString fileName = QFileDialog::getOpenFileName(this,
                                                        tr("Open File"), path_);
  return (OpenFileFromPath(fileName));
}

void
ImagePanel::SlotRemove() {
  emit(DeleteMe(GetIndex()));
}

void ImagePanel::SlotZoom() {
  Zoom();
}

void ImagePanel::Zoom() {
  if (zoom_) {
    ZoomOut();
    zoom_ = false;
  }
  else {
    ZoomIn();
    zoom_ = true;
  }
}

void ImagePanel::ZoomIn() {
  imageArea_->setMinimumHeight(image_->GetImage()->height());
  imageArea_->setMinimumWidth(image_->GetImage()->width());
  imageArea_->setPixmap(QPixmap::fromImage(*(image_->GetImage())));
  scrollArea_->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  scrollArea_->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  QScrollBar* v_scroll = scrollArea_->verticalScrollBar();
  QScrollBar* h_scroll = scrollArea_->horizontalScrollBar();
  int scroll_x, scroll_y;
  CalculateScroll(imageArea_->width(), imageArea_->height(), imageArea_->GetX(), imageArea_->GetY(),
                  scrollArea_->width(), scrollArea_->height(), scroll_x, scroll_y);
  h_scroll->setValue(scroll_x);
  v_scroll->setValue(scroll_y);
  ImageChangedInfo info;
  info.zoomed_ = true;
  info.direction_ = ImageChangedInfo::BOTH;
  info.lengths_.push_back(scroll_x);
  info.lengths_.push_back(scroll_y);
  Notify(ImageZoomedReason(), &info);
}

void ImagePanel::ZoomOut() {
  imageArea_->setPixmap(QPixmap::fromImage(*(image_->GetThumb())));
  imageArea_->setFixedSize(image_->GetThumb()->size() - QSize(2, 2));
  scrollArea_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  scrollArea_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  ImageChangedInfo info;
  info.zoomed_ = false;
  Notify(ImageZoomedReason(), &info);
}

void ImagePanel::HorizontalSliderMoved(int position) {
  ImageChangedInfo info;
  info.direction_ = ImageChangedInfo::HORIZONTAL;
  info.length_ = position;
  Notify(ImageScrolledReason(), &info);
}

void ImagePanel::VerticalSliderMoved(int position) {
  ImageChangedInfo info;
  info.direction_ = ImageChangedInfo::VERTICAL;
  info.length_ = position;
  Notify(ImageScrolledReason(), &info);
}

void ImagePanel::MoveHorizontalSlider(int length) {
  QScrollBar* h_scroll = scrollArea_->horizontalScrollBar();
  //assert(h_scroll);
  h_scroll->setValue(length);
}

void ImagePanel::MoveVerticalSlider(int length) {
  QScrollBar* v_scroll = scrollArea_->verticalScrollBar();
  //assert(v_scroll);
  v_scroll->setValue(length);
}

ImageProxy*
ImagePanel::GetImage() {
  return image_.get();
}

void
ImagePanel::UpdateWidth(int width) {
  if (!image_->GetThumb()->isNull()) {
    imageArea_->setPixmap(QPixmap::fromImage(*(image_->GetThumb(width))));
    imageArea_->setFixedSize(image_->GetThumb()->size() - QSize(2, 2));
    scrollArea_->setFixedSize(image_->GetThumb()->size() - QSize(2, 2));
  }
  else {
    imageArea_->setFixedSize(QSize(width, width * 3 / 4));
    scrollArea_->setFixedSize(QSize(width, width * 3 / 4));
  }
}

QString ImagePanel::GetImageFileName() {
  return image_->GetFileName();
}
