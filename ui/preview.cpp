/*
 * Preview.cpp
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#include "ui/preview.h"
#include "ui/imagelabel.h"

Preview::Preview(QWidget *parent)
  : QWidget(parent) {
  imagelabel_ = new ImageLabel(this);
  imagelabel_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  imagelabel_->setBackgroundRole( QPalette::Dark );
  
}

Preview::~Preview() {
}

void
Preview::SetImage(QImage image) {
  imagelabel_->setPixmap(QPixmap::fromImage(image));
  imagelabel_->setScaledContents(false);
  imagelabel_->setFixedSize(QSize(image.width(), image.height()));
}
