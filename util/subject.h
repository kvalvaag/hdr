/*
 * subject.h
 *
 *  Created on: 19. feb. 2013
 *      Author: petter
 */

#ifndef SUBJECT_H_
#define SUBJECT_H_

#include <vector>
#include <util/reason.h>

#include <util_export.h>

class Observer;
class UpdateInfo;

class UTIL_EXPORT Subject {
public:
  Subject();
  virtual
  ~Subject();

  void Attach(Observer *observer);
  void Detach(Observer *observer);
  void Notify(Reason reason, UpdateInfo* info = 0);

private:
  std::vector<Observer*> observers_;
};

#endif /* SUBJECT_H_ */

