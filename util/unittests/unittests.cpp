#define BOOST_TEST_MODULE ImageConversion_Test
#include <boost/test/unit_test.hpp>
#include <opencv2/opencv.hpp>
#include <util/imageutil.h>
#include <QString>
#include <QImage>

/*! ImageFixture is a fixture for testing conversion of image formats.
    It facilitates checking that conversion back and forth between
    Qt and OpenCV formats works.
*/
struct ImageFixture {
  ImageFixture() {
    orig_cv_lena_ = cv::imread("lena.jpg", 3);
    q_lena_ = ImageUtil::Mat2QImage(orig_cv_lena_);
    back_conv_cv_lena_ = ImageUtil::QImage2Mat(q_lena_);
  }

  cv::Mat orig_cv_lena_;
  QImage q_lena_;
  cv::Mat back_conv_cv_lena_;
};

BOOST_AUTO_TEST_CASE( Dummy_Test ) {
  BOOST_CHECK(1 + 1 == 2);
}

BOOST_AUTO_TEST_CASE(ImageConversion_Test) {
  ImageFixture imagefixture;
  cv::Mat diff = imagefixture.orig_cv_lena_ != imagefixture.back_conv_cv_lena_;
  cv::Mat channel1;
  cv::extractChannel(diff, channel1, 0);
  int test = cv::countNonZero(channel1);
  BOOST_CHECK(test == 0);
}
